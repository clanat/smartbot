﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SmartExtractor.Misc;
using System.IO;
using SmartExtractor.Core;

namespace SmartExtractor
{
    class Program
    {
        [STAThread]
        private static void Main(string[] args)
        {
            Console.CursorVisible = false;

            Console.WriteLine("Building NavData...");
            foreach (var path in Directory.EnumerateFiles("geometry", "*.mesh"))
            {
                var mesh = new TriangleMesh(File.ReadAllBytes(path));

                //mesh.ExportObjFile();

                NavData navData = null;

                try
                {
                    NavBuilder.BuildNavData(ref mesh, out navData);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
#if DEBUG
                    Console.WriteLine("Press to continue");
                    Console.ReadKey();
#endif
                    continue;
                }

                if (navData != null)
                {
                    navData.Export();
                }
            }

            Console.WriteLine("All done, press key to exit.");
            Console.ReadKey();
        }
    }
}
