﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartExtractor.Misc;
using Tripper.RecastManaged.Recast;
using Tripper.RecastManaged.Detour;
using Tripper.Tools.Math;

namespace SmartExtractor.Core
{
    public static class NavBuilder
    {
        public static class NavMeshSettings
        {
            private const float _agentHeight = 2.0f;
            private const float _agentMaxClimb = 0.9f;
            private const float _agentRadius = 0.6f;

            private const int _edgeLength = 12;
            private const int _regionMinSize = 8;
            private const int _regionMergeSize = 20;
            private const float _detailSampleDist = 6.0f;
            private const float _detailSampleMaxError = 1.0f;
            private const float _edgeMaxError = 1.3f;

            private static NavMeshCreateParams _params;

            public const float CellSize = Constants.UnitDimensions;
            public const float CellHeight = Constants.UnitDimensions;
            public const int WalkableSlopeAngle = 45;
            public static readonly int WalkableHeight = (int)(Math.Ceiling(_agentHeight / CellHeight));
            public static readonly int WalkableClimb = (int)(Math.Floor(_agentMaxClimb / CellHeight));
            public static readonly int WalkableRadius = (int)(Math.Ceiling(_agentRadius / CellHeight));
            public const float MaxSimplificationError = _edgeMaxError;
            public const int MinRegionArea = _regionMinSize ^ 2;
            public const int MergeRegionArea = _regionMergeSize ^ 2;
            public const int MaxVertsPerPoly = 6;
            public const float DetailSampleDist = _detailSampleDist < 0.9f ? 0 : CellSize * _detailSampleDist;
            public const float DetailSampleMaxError = CellHeight * _detailSampleMaxError;
            public const int MaxEdgeLength = (int)(_edgeLength / CellSize);


            public static NavMeshCreateParams NavMeshCreateParams
            {
                get
                {
                    if (_params == null)
                    {
                        _params = new NavMeshCreateParams();
                        _params.CellSize = CellSize;
                        _params.CellHeight = CellHeight;
                        _params.WalkableHeight = WalkableHeight;
                        _params.WalkableClimb = WalkableClimb;
                        _params.WalkableRadius = WalkableRadius;
                        _params.NVP = MaxVertsPerPoly;
                    }

                    return _params;
                }
            }
        }

        private static class NavMeshComponents
        {
            public static Heightfield Heightfield;
            public static CompactHeightfield CompactHeightfield;
            public static ContourSet ContourSet;
            public static PolyMesh PolyMesh;
            public static PolyMeshDetail PolyMeshDetail;
            
            public static void Clean()
            {
                if (Heightfield != null)
                {
                    Heightfield.Dispose();
                }

                if (CompactHeightfield != null)
                {
                    CompactHeightfield.Dispose();
                }

                if (ContourSet != null)
                {
                    ContourSet.Dispose();
                }

                if (PolyMesh != null)
                {
                    PolyMesh.Dispose();
                }

                if (PolyMeshDetail != null)
                {
                    PolyMeshDetail.Dispose();
                }

                Heightfield = null;
                CompactHeightfield = null;
                ContourSet = null;
                PolyMesh = null;
                PolyMeshDetail = null;
            }
        }

        public static unsafe void BuildNavData(ref TriangleMesh mesh, out NavData navData)
        {
            if (mesh.IsEmpty)
            {
                navData = null;
                return;
            }

            if (mesh.IsTiled)
            {
                Console.Write("{0} {1} [     ]", mesh.Map, mesh.Tile);
            }
            else
            {
                Console.Write("{0} [     ]", mesh.Map);
            }

            Console.SetCursorPosition(Console.CursorLeft - 6, Console.CursorTop);
            Console.Write("=");

            var bounds = new BoundingBox3();
            Recast.CalcBounds(mesh.SolidVertices.Concat(mesh.LiquidVertices).ToArray(), out bounds.Min, out bounds.Max);

            var settings = NavMeshSettings.NavMeshCreateParams;
            settings.BMin = bounds.Min;
            settings.BMax = bounds.Max;

            var height = 0;
            var width = 0;
            Recast.CalcGridSize(bounds.Min, bounds.Max, settings.CellSize, out width, out height);

            var success = Recast.CreateHeightfield(
                width, 
                height, 
                bounds.Min, 
                bounds.Max, 
                settings.CellSize, 
                settings.CellHeight, 
                out NavMeshComponents.Heightfield);

            if (!success)
            {
                throw new InvalidOperationException("Failed building height field");
            }

            var groundFlags = new byte[mesh.SolidIndices.Length / 3];
            for (int i = 0; i < groundFlags.Length; i++)
            {
                groundFlags[i] = (byte)AreaType.Ground;
            }

            Recast.MarkWalkableTriangles(NavMeshSettings.WalkableSlopeAngle, mesh.SolidVertices, mesh.SolidIndices, groundFlags);
            Recast.RasterizeTriangles(mesh.SolidVertices, mesh.SolidIndices, groundFlags, NavMeshComponents.Heightfield);

            Recast.FilterLowHangingWalkableObstacles(NavMeshSettings.WalkableClimb, NavMeshComponents.Heightfield);
            Recast.FilterLedgeSpans(NavMeshSettings.WalkableHeight, NavMeshSettings.WalkableClimb, NavMeshComponents.Heightfield);
            Recast.FilterWalkableLowHeightSpans(NavMeshSettings.WalkableHeight, NavMeshComponents.Heightfield);

            if (mesh.LiquidIndices.Length > 0)
            {
                Recast.RasterizeTriangles(mesh.LiquidVertices, mesh.LiquidIndices, mesh.LiquidTypes, NavMeshComponents.Heightfield);
            }

            Console.Write("=");

            success = Recast.BuildCompactHeightfield(
                NavMeshSettings.WalkableHeight, 
                NavMeshSettings.WalkableClimb, 
                NavMeshComponents.Heightfield, 
                out NavMeshComponents.CompactHeightfield);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed building compact height field");
            }

            success = Recast.ErodeWalkableArea(NavMeshSettings.WalkableRadius, NavMeshComponents.CompactHeightfield);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed eroding walkable area");
            }


            success = Recast.BuildDistanceField(NavMeshComponents.CompactHeightfield);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed building distance field");
            }

            success = Recast.BuildRegions(
                NavMeshComponents.CompactHeightfield, 
                0, 
                NavMeshSettings.MinRegionArea, 
                NavMeshSettings.MergeRegionArea);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed building regions");
            }

            Console.Write("=");
            success = Recast.BuildContours(
                NavMeshComponents.CompactHeightfield, 
                NavMeshSettings.MaxSimplificationError,
                NavMeshSettings.MaxEdgeLength, 
                out NavMeshComponents.ContourSet);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed building contours");
            }

            Console.Write("=");

            success = Recast.BuildPolyMesh(NavMeshComponents.ContourSet, settings.NVP, out NavMeshComponents.PolyMesh);

            if (!success)
            {
                NavMeshComponents.Clean();
                throw new InvalidOperationException("Failed building polymesh");
            }

            success = Recast.BuildPolyMeshDetail(
                NavMeshComponents.PolyMesh, 
                NavMeshComponents.CompactHeightfield, 
                NavMeshSettings.DetailSampleDist, 
                NavMeshSettings.DetailSampleMaxError, 
                out NavMeshComponents.PolyMeshDetail);

            if (!success)
            {
                throw new InvalidOperationException("Failed building polymesh detail");
            }

            NavMeshComponents.Heightfield.Dispose();
            NavMeshComponents.CompactHeightfield.Dispose();
            NavMeshComponents.ContourSet.Dispose();

            for (int i = 0; i < NavMeshComponents.PolyMesh.NPolys; i++)
            {
                if (((DefaultAreaType)NavMeshComponents.PolyMesh.Areas[i] & DefaultAreaType.RC_WALKABLE_AREA) != DefaultAreaType.RC_NULL_AREA)
                {
                    NavMeshComponents.PolyMesh.Flags[i] = NavMeshComponents.PolyMesh.Areas[i];
                }
            }

            settings.Verts = NavMeshComponents.PolyMesh.Verts;
            settings.VertCount = NavMeshComponents.PolyMesh.NVerts;
            settings.Polys = NavMeshComponents.PolyMesh.Polys;
            settings.PolyAreas = NavMeshComponents.PolyMesh.Areas;
            settings.PolyFlags = NavMeshComponents.PolyMesh.Flags;
            settings.PolyCount = NavMeshComponents.PolyMesh.NPolys;
            settings.NVP = NavMeshComponents.PolyMesh.NVP;
            settings.DetailMeshes = NavMeshComponents.PolyMeshDetail.Meshes;
            settings.DetailVerts = NavMeshComponents.PolyMeshDetail.Verts;
            settings.DetailVertsCount = NavMeshComponents.PolyMeshDetail.NVerts;
            settings.DetailTris = NavMeshComponents.PolyMeshDetail.Tris;
            settings.DetailTriCount = NavMeshComponents.PolyMeshDetail.NTris;


            byte[] data;
            Detour.CreateNavMeshData(settings, out data);

            //var navMesh = new NavMesh();
            
            //if (mesh.IsTiled)
            //{
            //    navMesh.Init(new NavMeshParams()
            //    {
            //        TileWidth = Constants.GridSize,
            //        TileHeight = Constants.GridSize,
            //        MaxPolys = 4096,
            //        MaxTiles = 16384,
            //        Origin = Vector3.Zero
            //    });

            //    TileReference tileRef;
            //    var status = navMesh.AddTile(data, out tileRef);

            //    if (status.Failed || tileRef.Id == 0)
            //    {
            //        Console.Write("Failed adding tile to tiled navmesh: {0} {1}", mesh.Map, mesh.Tile);
            //    }
            //}
            //else
            //{
            //    var status = navMesh.Init(data);
            //    var tileRef = navMesh.GetTileRef(navMesh.GetTile(0));

            //    if (status.Failed)
            //    {
            //        Console.Write("Failed adding tile to nontiled navmesh: {0} {1}", mesh.Map, mesh.Tile);
            //    }
            //}

            Console.Write("=");

            Console.WriteLine();

            //var start = new Vector3(2434.15771f, -13.8632488f, -46.7324448f);
            //var end = new Vector3(2470.65283f, -4.29718971f, -31.4430313f);

            //var extends = new Vector3(2, 20, 2);

            //var query = new NavMeshQuery();
            //query.Init(navMesh, 1024);

            //var filter = new QueryFilter()
            //{
            //    IncludeFlags = (ushort)AreaType.Ground | (ushort)DefaultAreaType.RC_WALKABLE_AREA
            //};

            //Vector3 point;
            //PolygonReference polyRef;
            //var s = query.FindNearestPolygon(start, extends, QueryFilter.Default, out point, out polyRef);



            NavMeshComponents.Clean();

            //navMesh.RemoveTile(navMesh.GetTileRef(navMesh.GetTile(0)));
            navData = mesh.IsTiled ? new NavData(mesh.Map, data, mesh.Tile) : new NavData(mesh.Map, data);
        }
    }
}
