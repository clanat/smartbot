﻿using SmartExtractor.Misc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SmartExtractor.Core
{
    public static class Extensions
    {
        public static byte[] SerializeStruct<T>(T s) where T : struct
        {
            var size = Marshal.SizeOf(typeof(T));
            var array = new byte[size];
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(s, ptr, true);
            Marshal.Copy(ptr, array, 0, size);
            Marshal.FreeHGlobal(ptr);
            return array;
        }

        public static T DeserializeStruct<T>(byte[] array) where T : struct
        {
            var size = Marshal.SizeOf(typeof(T));
            var ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(array, 0, ptr, size);
            var s = (T)Marshal.PtrToStructure(ptr, typeof(T));
            Marshal.FreeHGlobal(ptr);
            return s;
        }

        public static T ReadStruct<T>(BinaryReader reader) where T :struct
        {
            var size = Marshal.SizeOf(typeof(T));
            return (T)(DeserializeStruct<T>(reader.ReadBytes(size)));
        }

        public static void WriteStruct<T>(BinaryWriter writer, T theStruct) where T : struct
        {
            writer.Write(SerializeStruct<T>(theStruct));
        }

        public static float[] ReadFromBuffer(byte[] buffer)
        {
            var array = new float[buffer.Length / 4];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = BitConverter.ToSingle(buffer, i * 4);
            }

            return array;
        }
    }
}
