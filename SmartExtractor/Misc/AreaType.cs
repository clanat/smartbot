﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartExtractor.Misc
{
    [Flags]
    public enum AreaType : byte
    {
        Empty = 0x00,
        Ground = 0x01,
        Magma = 0x02,
        Slime = 0x04,
        Water = 0x08,
        Unused1 = 0x10,
        Unused2 = 0x20,
        Unused3 = 0x40,
        Unused4 = 0x80
    }
}
