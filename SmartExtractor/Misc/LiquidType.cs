﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartExtractor.Misc
{
    public enum LiquidType
    {
        Empty = 0x00,
        Water = 0x01,
        Ocean = 0x02,
        Magma = 0x04,
        Slime = 0x08,
        DarkWater = 0x10,
        WMOWater = 0x020
    }
}
