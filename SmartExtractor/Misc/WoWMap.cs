﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartExtractor.Misc
{
    public class WoWMap
    {
        public string Name { get; private set; }

        public WoWMap(WoWInternalMaps map)
        {
            this.Name = Constants.WoWMapList[(int)map];
        }
    }
}
