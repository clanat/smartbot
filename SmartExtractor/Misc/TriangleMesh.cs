﻿using SmartExtractor.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tripper.Tools.Math;


namespace SmartExtractor.Misc
{
    public class TriangleMesh
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct TileMeshHeader
        {
            public int MapId;
            [MarshalAs(UnmanagedType.I1)]
            public bool IsTiled;
            public int TileX;
            public int TileY;
            public int SolidVertsSize;
            public int TerrainIndicesSize;
            public int LiquidVertsSize;
            public int LiquidTrisSize;
            public int LiquidTypesSize;
        }

        public WoWInternalMaps Map { get; private set; }
        public bool IsTiled { get; private set; }
        public Vector2i Tile { get; private set; }
        public Vector3[] SolidVertices { get; private set; }
        public int[] SolidIndices { get; private set; }
        public Vector3[] LiquidVertices { get; private set; }
        public int[] LiquidIndices { get; private set; }
        public byte[] LiquidTypes { get; private set; }

        public bool IsEmpty { get { return SolidVertices.Length + LiquidVertices.Length == 0; } }

        public TriangleMesh(byte[] data)
        {
            using (var stream = new MemoryStream(data))
            {
                using (var reader = new BinaryReader(stream))
                {
                    var header = Extensions.ReadStruct<TileMeshHeader>(reader);

                    SolidVertices = new Vector3[header.SolidVertsSize / 3];
                    SolidIndices = new int[header.TerrainIndicesSize];
                    LiquidVertices = new Vector3[header.LiquidVertsSize / 3];
                    LiquidIndices = new int[header.LiquidTrisSize];
                    LiquidTypes = new byte[header.LiquidTypesSize];

                    Map = (WoWInternalMaps)header.MapId;
                    IsTiled = header.IsTiled;
                    Tile = new Vector2i(header.TileX, header.TileY);

                    if (SolidVertices.Length > 0)
                    {
                        for (int i = 0; i < SolidVertices.Length; i++)
                        {
                            SolidVertices[i] = new Vector3()
                            {
                                X = reader.ReadSingle(),
                                Y = reader.ReadSingle(),
                                Z = reader.ReadSingle()
                            };
                        }
                    }

                    if (SolidIndices.Length > 0)
                    {
                        for (int i = 0; i < SolidIndices.Length; i++)
                        {
                            SolidIndices[i] = reader.ReadInt32();
                        }
                    }

                    if (LiquidVertices.Length > 0)
                    {
                        for (int i = 0; i < LiquidVertices.Length; i++)
                        {
                            LiquidVertices[i] = new Vector3()
                            {
                                X = reader.ReadSingle(),
                                Y = reader.ReadSingle(),
                                Z = reader.ReadSingle()
                            };
                        }
                    }

                    if (LiquidIndices.Length > 0)
                    {
                        for (int i = 0; i < LiquidIndices.Length; i++)
                        {
                            LiquidIndices[i] = reader.ReadInt32();
                        }
                    }

                   

                    if (LiquidTypes.Length > 0)
                    {
                        for (int i = 0; i < LiquidTypes.Length; i++)
                        {
                            LiquidTypes[i] = reader.ReadByte();
                        }
                    }
                }
            }
        }

        public void ExportToObjFile()
        {
            System.Globalization.CultureInfo customCulture =
                        (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            var allVerts = SolidVertices.Concat(LiquidVertices).ToArray();
            var allIndices = SolidIndices.Concat(LiquidIndices).ToArray();
            var objFileLines = new List<string>();

            for (int i = 0; i < allVerts.Length; i++)
            {
                objFileLines.Add(string.Format("v {0} {1} {2}", allVerts[i].X, allVerts[i].Y, allVerts[i].Z));
            }

            for (int i = 0; i < allIndices.Length / 3; i++)
            {
                objFileLines.Add(string.Format("f {0} {1} {2}", allIndices[i * 3] + 1, allIndices[i * 3 + 1] + 1, allIndices[i * 3 + 2] + 1));
            }

            if (objFileLines.Count > 0)
            {
                var fileName = IsTiled
                    ? string.Format("{0}_{1}_{2}.obj", Constants.WoWMapList[(int)Map], Tile.X, Tile.Y)
                    : string.Format("{0}.obj", Constants.WoWMapList[(int)Map]);

                using (var writer = new StreamWriter(File.OpenWrite(fileName)))
                {
                    foreach (var line in objFileLines)
                    {
                        writer.WriteLine(line);
                    }
                }
            }
        }
    }
}
