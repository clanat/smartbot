﻿using SmartExtractor.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tripper.Tools.Math;

namespace SmartExtractor.Misc
{
    public class NavData
    {
        public struct NavDataHeader
        {
            public readonly int MapId;
            public readonly bool IsTiled;
            public readonly int TileX;
            public readonly int TileY;
            public readonly int DataSize;

            public NavDataHeader(int mapId, bool isTiled, int tileX, int tileY, int dataSize)
            {
                MapId = mapId;
                IsTiled = isTiled;
                TileX = tileX;
                TileY = tileY;
                DataSize = dataSize;
            }
        }


        public WoWInternalMaps Map { get; private set; }
        public bool IsTiled { get; private set; }
        public Vector2i Tile { get; private set; }
        public byte[] Data { get; private set; }
        public NavDataHeader Header { get; private set; }

        private NavData(WoWInternalMaps map, byte[] data, bool isTiled, Vector2i tile)
        {
            Map = map;
            IsTiled = isTiled;
            Tile = tile;
            Data = data;
            Header = new NavDataHeader((int)map, isTiled, tile.X, tile.Y, data.Length);
        }

        public NavData(WoWInternalMaps map, byte[] data) : this(map, data, false, new Vector2i(64, 64)) { }
        public NavData(WoWInternalMaps map, byte[] data, Vector2i tile) : this(map, data, true, tile) { }
        

        public void Export()
        {
            if (Header.DataSize == 0)
            {
                return;
            }

            var fileName = IsTiled
                ? string.Format("{0}_{1}_{2}.ndata", Constants.WoWMapList[Header.MapId], Tile.X, Tile.Y)
                : string.Format("{0}.ndata", Constants.WoWMapList[Header.MapId]);

            using (var stream = File.OpenWrite(fileName))
            {
                using (var writer = new BinaryWriter(stream))
                {
                    Extensions.WriteStruct<NavDataHeader>(writer, Header);
                    writer.Write(Data);
                }
            }
        }

    }
}
